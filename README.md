This project used to play Dizzywing Dispatch from the game Feral (Now defunct) automatically and predict as many moves ahead as it could to choose the most efficient moves each time. It controls the mouse using OS calls and has no ingame hooks. The game window must be the smallest resolution possible for this to work.

The bot playing the game:

![Dizzywing Dispatch being played by the program](http://zenny3d.com/vid/dizzywingbotsm.gif)

[View Higher Res](https://gfycat.com/SociableBowedHoneybadger.gif)
