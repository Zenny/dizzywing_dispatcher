use crate::window::{Rect, Window};

//use inputbot::{MouseButton, MouseCursor};

use crate::gamegrid::{GameGrid, LocalMove, GRID_SIZE, SQUARE_DIM, MoveInfo};
use crate::pixelmap::{PixelMap, Pixel};
use std::ffi::CString;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::time::Duration;
use enigo::{Enigo, MouseControllable, MouseButton};
use bytes::BytesMut;

pub const GRID_OFFSET: (i32, i32) = (174, 51);

#[derive(Debug)]
pub struct Dizzy {
    pub win: Window,
    pub grid_area: Rect,
    pub game_grid: GameGrid,
    stop: Arc<AtomicBool>,
    enigo: Enigo,
}

pub fn move_abs(left: i32, top: i32) {
    //MouseCursor.move_abs(0, 0);
    //MouseCursor.move_rel(left, top);
}

impl Dizzy {
    pub fn new(win: Window, stop: Arc<AtomicBool>) -> Self {
        Self {
            grid_area: Rect {
                left: win.area.left + GRID_OFFSET.0,
                top: win.area.top + GRID_OFFSET.1,
                width: i32::from(SQUARE_DIM) * i32::from(GRID_SIZE),
                height: i32::from(SQUARE_DIM) * i32::from(GRID_SIZE),
            },
            win,
            game_grid: GameGrid::new(),
            stop,
            enigo: Enigo::new(),
        }
    }

    pub fn play(&mut self) -> Result<(), ()> {
        // init
        self.win.topmost(true);
        let bmp = Window::try_get_bitmap(&mut self.win,
                GRID_OFFSET.0,
                GRID_OFFSET.1,
                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
            );

        let pxmap = PixelMap::from(&bmp, usize::from(SQUARE_DIM) * 9);

        //let aa = pxmap.get_range((0, 0), (74 * 9, 74 * 9)).unwrap();

        //let mut f = File::create("S:\\testfull.raw").unwrap();
        //f.write_all(&aa).expect("Failed to write raw");
        println!("Starting...");

        'main: while !self.stop.load(Ordering::SeqCst) {
            self.game_grid.has_unk = true;
            while self.game_grid.has_unk {
                self.win.update_pos().expect("Failed to update window pos");
                self.game_grid.update_real(&mut self.win);
                if self.stop.load(Ordering::SeqCst) {
                    break 'main;
                }
            }
            unsafe {
                libc::system(CString::new("cls").unwrap().as_ptr());
                // Why did all of the packages have to be broken?
            }
            println!("{}", self.game_grid);

            let bmp = Window::try_get_bitmap(&mut self.win,
                928,
                700,
                1,
                1);

            let pxmap = PixelMap::from(&bmp, 1);
            let p = pxmap.get(0,0).unwrap().clone();
            let has_goal = p == Pixel {
                r: 106,
                g: 45,
                b: 86
            };
            println!("has goal: {} {:?}", has_goal, p);
            let moves = self.game_grid.find_moves();
            let mut filtered: Vec<MoveInfo> = moves.iter().filter_map(|m| {
                let b1 = self.game_grid.get(m.pos.0, m.pos.1).unwrap();
                let b2 = self.game_grid.get((m.pos.0 as i8 + m.dir.0) as usize, (m.pos.1 as i8 + m.dir.1) as usize).unwrap();

                if ((b1.is_always_movable() || b2.is_always_movable()) && !has_goal) || b1.is_egg() || b2.is_egg() {
                    None
                } else {
                    Some(m.clone())
                }
            }).collect();

            // TODO: Dizzy bird first?
            if filtered.len() == 0 && moves.len() > 0 {
                println!("Filtered had no moves");
                filtered = moves.iter().filter_map(|m| {
                    let b1 = self.game_grid.get(m.pos.0, m.pos.1).unwrap();
                    let b2 = self.game_grid.get((m.pos.0 as i8 + m.dir.0) as usize, (m.pos.1 as i8 + m.dir.1) as usize).unwrap();

                    if b1.is_egg() || b2.is_egg() {
                        None
                    } else {
                        Some(m.clone())
                    }
                }).collect();
            }

            let mut best = LocalMove::default();
            println!("Found possible moves: {}", filtered.len());

            if filtered.len() > 0 {
                let mut best_mv = filtered.first().unwrap().to_owned();
                for mv in filtered {
                    let mut local = self.game_grid.clone();
                    let details = local.play_move_local(&mv);
                    println!("{:?}\t{:?}", mv, details);
                    if let Some(std::cmp::Ordering::Greater) = details.partial_cmp(&best) {
                        best = details;
                        best_mv = mv;
                    }
                }
                println!("Best move: {:?}", best_mv);

                self.enigo.mouse_move_to(
                    self.grid_area.left + (best_mv.pos.0 as i32 * i32::from(SQUARE_DIM) + i32::from(SQUARE_DIM) / 2),
                    self.grid_area.top + (best_mv.pos.1 as i32 * i32::from(SQUARE_DIM) + i32::from(SQUARE_DIM) / 2),
                );
                std::thread::sleep(Duration::from_millis(20));
                self.enigo.mouse_down(MouseButton::Left);
                std::thread::sleep(Duration::from_millis(20));
                for _ in 0..SQUARE_DIM {
                    self.enigo.mouse_move_relative(best_mv.dir.0 as i32, best_mv.dir.1 as i32);
                    std::thread::sleep(Duration::from_millis(1));
                }
                self.enigo.mouse_up(MouseButton::Left);

                std::thread::sleep(Duration::from_millis((870 * best.steps) as u64));
            } else {
                // Click dizzy bird
                self.enigo.mouse_move_to(
                    self.grid_area.left + 755,
                    self.grid_area.top + 262,
                );
                self.enigo.mouse_down(MouseButton::Left);
                std::thread::sleep(Duration::from_millis(25));
                self.enigo.mouse_up(MouseButton::Left);
            }
        }
        println!("End");
        Ok(())
    }
}

impl Drop for Dizzy {
    fn drop(&mut self) {
        self.win.topmost(false);
    }
}
