///! Converts winapi bitmaps into a map of managable pixels.

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Pixel {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

#[derive(Debug, Clone)]
pub struct PixelMap {
    pub row_w: usize,
    pub inner: Vec<Pixel>,
}

impl PixelMap {
    /// w: width of a row in pixels
    pub fn from(buf: &[u8], w: usize) -> Self {
        let mut ret = Vec::with_capacity(buf.len());

        // Winapi gives us 4 channels when we only need RGB
        for px in buf.chunks_exact(4) {
            ret.push(Pixel {
                r: px[2],
                g: px[1],
                b: px[0],
            });
        }

        Self {
            row_w: w,
            inner: ret,
        }
    }

    pub fn get(&self, x: usize, y: usize) -> Option<&Pixel> {
        self.inner.get(x + self.row_w * y)
    }

    /// Returns None when range out of bounds
    pub fn get_range(&self, tl: (usize, usize), wh: (usize, usize)) -> Option<Vec<u8>> {
        let mut ret = Vec::with_capacity(wh.0 * wh.1);
        let start = tl.0 + tl.1 * self.row_w;

        for y in 0..wh.1 {
            for x in 0..wh.0 {
                if let Some(px) = self.inner.get(start + (y * self.row_w) + x) {
                    ret.push(px.r);
                    ret.push(px.g);
                    ret.push(px.b);
                } else {
                    return None;
                }
            }
        }
        Some(ret)
    }

    /// Returns None when range out of bounds
    pub fn get_pixel_range(&self, tl: (usize, usize), wh: (usize, usize)) -> Option<Vec<&Pixel>> {
        let mut ret = Vec::with_capacity(wh.0 * wh.1);
        let start = tl.0 + tl.1 * self.row_w;

        for y in 0..wh.1 {
            for x in 0..wh.0 {
                if let Some(px) = self.inner.get(start + (y * self.row_w) + x) {
                    ret.push(px);
                } else {
                    return None;
                }
            }
        }
        Some(ret)
    }

    /// Returns None when range out of bounds
    pub fn avg_from_range(&self, tl: (usize, usize), wh: (usize, usize)) -> Option<Pixel> {
        if let Some(v) = self.get_pixel_range(tl, wh) {
            let mut tmpr: usize = 0;
            let mut tmpg: usize = 0;
            let mut tmpb: usize = 0;

            let len = v.len();

            for px in v {
                tmpr += usize::from(px.r);
                tmpg += usize::from(px.g);
                tmpb += usize::from(px.b);
            }
            Some(Pixel {
                r: (tmpr / len) as u8,
                g: (tmpg / len) as u8,
                b: (tmpb / len) as u8,
            })
        } else {
            None
        }
    }
}
