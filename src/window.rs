use std::mem::size_of;
use winapi::shared::windef::{HWND, POINT, RECT};
use winapi::um::wingdi;
use winapi::um::winuser;

use std::ffi::CString;

use self::WinFindError::*;
use self::WinImgError::*;
use crate::is_null;
use bytes::BytesMut;
use winapi::shared::minwindef::LPVOID;

#[derive(Debug)]
pub enum WinFindError {
    WinNotFound,
    RectNotFound,
}

#[derive(Debug)]
pub enum WinImgError {
    ImageFailed,
}

#[derive(Debug, PartialEq, Eq)]
pub struct Rect {
    pub left: i32,
    pub top: i32,
    pub width: i32,
    pub height: i32,
}

#[derive(Debug)]
pub struct Window {
    hnd: HWND,
    pub area: Rect,
}

impl Window {
    pub fn from_title(title: &str) -> Result<Self, WinFindError> {
        unsafe {
            let title = CString::new(title).unwrap();
            let hnd = winuser::FindWindowA(std::ptr::null_mut(), title.as_ptr());

            if is_null(hnd) {
                return Err(WinNotFound);
            }

            Ok(Self {
                area: Self::find_pos(hnd.clone())?,
                hnd,
            })
        }
    }

    pub fn find_pos(hnd: HWND) -> Result<Rect, WinFindError> {
        unsafe {
            let mut winrect: RECT = RECT {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
            };

            let mut clirect: RECT = RECT {
                left: 0,
                top: 0,
                right: 0,
                bottom: 0,
            };
            let a = winuser::GetWindowRect(hnd, &mut winrect)
                | winuser::GetClientRect(hnd, &mut clirect);
            if a <= 0 {
                return Err(RectNotFound);
            }

            let mut ltpnt = POINT {
                x: clirect.left,
                y: clirect.top,
            };

            // Does this account for SM_XVIRTUALSCREEN?
            let a = winuser::ClientToScreen(hnd, &mut ltpnt);
            if a <= 0 {
                return Err(RectNotFound);
            }

            let lborder = ltpnt.x - winrect.left;
            let tborder = ltpnt.y - winrect.top;
            Ok(Rect {
                left: winrect.left + lborder,
                top: winrect.top + tborder,
                width: clirect.right,
                height: clirect.bottom,
            })
        }
    }

    /// Returns a BGR bitmap
    pub fn get_bitmap(&self, x: i32, y: i32, w: u32, h: u32) -> Result<BytesMut, WinImgError> {
        unsafe {
            //let virtscr_x = winuser::GetSystemMetrics(winuser::SM_XVIRTUALSCREEN);
            //let virtscr_y = winuser::GetSystemMetrics(winuser::SM_YVIRTUALSCREEN);
            let dc_scr = winuser::GetDC(std::ptr::null_mut());
            let dc_mem = wingdi::CreateCompatibleDC(dc_scr);
            let bmp = wingdi::CreateCompatibleBitmap(dc_scr, w as i32, h as i32);
            let old_bmp = wingdi::SelectObject(dc_mem, bmp as LPVOID);
            wingdi::BitBlt(
                dc_mem,
                0,
                0,
                w as i32,
                h as i32,
                dc_scr,
                x + self.area.left,
                y + self.area.top,
                wingdi::SRCCOPY,
            );
            wingdi::SelectObject(dc_mem, old_bmp);

            let hdc = wingdi::CreateCompatibleDC(std::ptr::null_mut());

            let mut info = wingdi::BITMAPINFO {
                bmiHeader: wingdi::BITMAPINFOHEADER {
                    biSize: size_of::<wingdi::BITMAPINFOHEADER>() as u32,
                    biWidth: w as i32,
                    biHeight: -(h as i32), // Window bitmaps are bottom line first so reverse
                    biPlanes: 1,
                    biBitCount: 32,
                    biCompression: wingdi::BI_RGB,
                    biSizeImage: 0,
                    biXPelsPerMeter: 0,
                    biYPelsPerMeter: 0,
                    biClrUsed: 0,
                    biClrImportant: 0,
                },
                bmiColors: [wingdi::RGBQUAD {
                    rgbBlue: 0,
                    rgbGreen: 0,
                    rgbRed: 0,
                    rgbReserved: 0,
                }],
            };

            // Get info first
            wingdi::GetDIBits(
                hdc,
                bmp,
                0,
                h,
                std::ptr::null_mut(),
                &mut info,
                wingdi::DIB_RGB_COLORS,
            );

            let mut buf = BytesMut::with_capacity(info.bmiHeader.biSizeImage as usize);
            buf.set_len(info.bmiHeader.biSizeImage as usize);

            if wingdi::GetDIBits(
                hdc,
                bmp,
                0,
                h,
                buf.as_mut_ptr() as LPVOID,
                &mut info,
                wingdi::DIB_RGB_COLORS,
            ) == 0
            {
                wingdi::SelectObject(dc_mem, old_bmp);
                wingdi::DeleteObject(bmp as LPVOID);
                wingdi::DeleteDC(dc_mem);
                winuser::ReleaseDC(std::ptr::null_mut(), dc_scr);
                Err(ImageFailed)
            } else {
                wingdi::SelectObject(dc_mem, old_bmp);
                wingdi::DeleteObject(bmp as LPVOID);
                wingdi::DeleteDC(dc_mem);
                winuser::ReleaseDC(std::ptr::null_mut(), dc_scr);
                Ok(buf)
            }
        }
    }

    pub fn try_get_bitmap(win: &mut Window, x: i32, y: i32, w: u32, h: u32) -> BytesMut {
        match win
            .get_bitmap(x,y,w,h) {
            Ok(v) => v,
            Err(e) => {
                println!("ERROR: {:?}", e);
                *win = crate::get_win(); // Try to recover
                win.get_bitmap(x,y,w,h).expect("Failed to recover from bmp error")
            },
        }
    }

    pub fn update_pos(&mut self) -> Result<(), WinFindError> {
        let pos = Self::find_pos(self.hnd.clone())?;
        if pos != self.area {
            self.area = pos;
            println!("Updated pos to {:?}", self.area);
        }
        Ok(())
    }

    pub fn topmost(&self, on: bool) {
        unsafe {
            if on {
                winuser::SetWindowPos(
                    self.hnd,
                    winuser::HWND_TOPMOST,
                    0,
                    0,
                    0,
                    0,
                    winuser::SWP_NOMOVE | winuser::SWP_NOSIZE | winuser::SWP_SHOWWINDOW,
                );
            } else {
                winuser::SetWindowPos(
                    self.hnd,
                    winuser::HWND_NOTOPMOST,
                    0,
                    0,
                    0,
                    0,
                    winuser::SWP_NOMOVE | winuser::SWP_NOSIZE | winuser::SWP_SHOWWINDOW,
                );
            }
        }
    }
}
