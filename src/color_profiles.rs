use crate::gamegrid::{Bird, ItemType};
use crate::pixelmap::Pixel;

/////////////////////////
// NORMAL
/////////////////////////
#[rustfmt::skip]
const PURPLE_BIRD: [Pixel;2] = [
    Pixel { r: 145, g: 7, b: 209 },
    Pixel { r: 146, g: 7, b: 211 },
];

#[rustfmt::skip]
const GREEN_BIRD: [Pixel;2] = [
    Pixel { r: 73, g: 178, b: 4 },
    Pixel { r: 74, g: 180, b: 5 }
];

#[rustfmt::skip]
const RED_BIRD: [Pixel;2] = [
    Pixel { r: 199, g: 25, b: 69 },
    Pixel { r: 201, g: 25, b: 69 },
];

#[rustfmt::skip]
const PINK_BIRD: [Pixel;1] = [
    Pixel { r: 254, g: 91, b: 161 },
];

#[rustfmt::skip]
const WHITE_BIRD: [Pixel;2] = [
    Pixel { r: 229, g: 228, b: 230 },
    Pixel { r: 228, g: 227, b: 228 },
];

#[rustfmt::skip]
const YELLOW_BIRD: [Pixel;2] = [
    Pixel { r: 251, g: 204, b: 64 },
    Pixel { r: 252, g: 206, b: 65 },
];

#[rustfmt::skip]
const DBLUE_BIRD: [Pixel;1] = [
    Pixel { r: 66, g: 60, b: 186 },
];

#[rustfmt::skip]
const BLINK_BIRD: [Pixel;7] = [
    Pixel { r: 201, g: 167, b: 254 },
    Pixel { r: 199, g: 166, b: 254 },
    Pixel { r: 197, g: 168, b: 254 },
    Pixel { r: 195, g: 168, b: 254 },
    Pixel { r: 199, g: 168, b: 254 },
    Pixel { r: 203, g: 166, b: 254 },
    Pixel { r: 193, g: 169, b: 254 }
];

/////////////////////////
// SPECIAL
/////////////////////////
#[rustfmt::skip]
const RAINBOW_BIRD: [Pixel;23] = [
    Pixel { r: 191, g: 143, b: 224 },
    Pixel { r: 188, g: 135, b: 228 },
    Pixel { r: 189, g: 139, b: 226 },
    Pixel { r: 184, g: 134, b: 221 },
    Pixel { r: 192, g: 143, b: 225 },
    Pixel { r: 188, g: 137, b: 227 },
    Pixel { r: 185, g: 136, b: 222 },
    Pixel { r: 190, g: 143, b: 222 },
    Pixel { r: 193, g: 138, b: 227 },
    Pixel { r: 192, g: 136, b: 227 },
    Pixel { r: 196, g: 144, b: 224 },
    Pixel { r: 186, g: 136, b: 225 },
    Pixel { r: 194, g: 143, b: 224 },
    Pixel { r: 188, g: 136, b: 222 },
    Pixel { r: 190, g: 135, b: 227 },
    Pixel { r: 190, g: 135, b: 222 },
    Pixel { r: 190, g: 137, b: 225 },
    Pixel { r: 195, g: 140, b: 226 },
    Pixel { r: 190, g: 137, b: 227 },
    Pixel { r: 192, g: 136, b: 222 },
    Pixel { r: 191, g: 139, b: 227 },
    Pixel { r: 190, g: 137, b: 227 },
    Pixel { r: 188, g: 140, b: 221 },
];

#[rustfmt::skip]
const ITEM: [Pixel;8] = [
    Pixel { r: 186, g: 128, b: 44 },
    Pixel { r: 188, g: 129, b: 44 },
    Pixel { r: 189, g: 130, b: 44 },
    Pixel { r: 183, g: 125, b: 43 },
    Pixel { r: 228, g: 149, b: 30 },
    Pixel { r: 191, g: 132, b: 44 },
    Pixel { r: 226, g: 148, b: 30 },
    Pixel { r: 184, g: 127, b: 44 },
];

/////////////////////////
// UP
/////////////////////////

#[rustfmt::skip]
const YELLOW_UP: [Pixel;4] = [
    Pixel { r: 254, g: 234, b: 91 },
    Pixel { r: 254, g: 236, b: 92 },
    Pixel { r: 255, g: 238, b: 94 },
    Pixel { r: 255, g: 239, b: 95 },
];

#[rustfmt::skip]
const GREEN_UP: [Pixel;5] = [
    Pixel { r: 62, g: 160, b: 0 },
    Pixel { r: 59, g: 155, b: 0 },
    Pixel { r: 61, g: 159, b: 0 },
    Pixel { r: 58, g: 153, b: 0 },
    Pixel { r: 57, g: 151, b: 0 },
];

#[rustfmt::skip]
const PURPLE_UP: [Pixel;5] = [
    Pixel { r: 195, g: 2, b: 242 },
    Pixel { r: 193, g: 2, b: 240 },
    Pixel { r: 199, g: 2, b: 245 },
    Pixel { r: 197, g: 2, b: 243 },
    Pixel { r: 202, g: 2, b: 247 },
];

#[rustfmt::skip]
const PINK_UP: [Pixel;4] = [
    Pixel { r: 232, g: 77, b: 152 },
    Pixel { r: 234, g: 79, b: 153 },
    Pixel { r: 238, g: 82, b: 155 },
    Pixel { r: 230, g: 76, b: 151 },
];

#[rustfmt::skip]
const WHITE_UP: [Pixel;6] = [
    Pixel { r: 233, g: 232, b: 234 },
    Pixel { r: 226, g: 225, b: 227 },
    Pixel { r: 224, g: 224, b: 226 },
    Pixel { r: 222, g: 222, b: 224 },
    Pixel { r: 227, g: 227, b: 229 },
    Pixel { r: 224, g: 223, b: 226 },
];

#[rustfmt::skip]
const RED_UP: [Pixel;3] = [
    Pixel { r: 230, g: 25, b: 60 },
    Pixel { r: 232, g: 25, b: 60 },
    Pixel { r: 229, g: 25, b: 60 },
];

#[rustfmt::skip]
const BLINK_UP: [Pixel;3] = [
    Pixel { r: 125, g: 247, b: 251 },
    Pixel { r: 132, g: 250, b: 253 },
    Pixel { r: 123, g: 246, b: 251 },
];

#[rustfmt::skip]
const DBLUE_UP: [Pixel;2] = [
    Pixel { r: 74, g: 86, b: 240 },
    Pixel { r: 73, g: 85, b: 238 },
];

/////////////////////////
// SIDE
/////////////////////////
#[rustfmt::skip]
const RED_SIDE: [Pixel;9] = [
    Pixel { r: 245, g: 40, b: 98 },
    Pixel { r: 219, g: 30, b: 64 },
    Pixel { r: 196, g: 34, b: 67 },
    Pixel { r: 222, g: 29, b: 63 },
    Pixel { r: 203, g: 29, b: 63 },
    Pixel { r: 165, g: 41, b: 74 },
    Pixel { r: 218, g: 30, b: 64 },
    Pixel { r: 190, g: 35, b: 69 },
    Pixel { r: 192, g: 35, b: 69 },
];

#[rustfmt::skip]
const GREEN_SIDE: [Pixel;6] = [
    Pixel { r: 54, g: 141, b: 1 },
    Pixel { r: 33, g: 113, b: 0 },
    Pixel { r: 56, g: 145, b: 0 },
    Pixel { r: 45, g: 130, b: 0 },
    Pixel { r: 55, g: 143, b: 0 },
    Pixel { r: 43, g: 128, b: 0 }
];

#[rustfmt::skip]
const DBLUE_SIDE: [Pixel;5] = [
    Pixel { r: 74, g: 89, b: 255 },
    Pixel { r: 128, g: 148, b: 255 },
    Pixel { r: 69, g: 79, b: 248 },
    Pixel { r: 77, g: 92, b: 255 },
    Pixel { r: 65, g: 74, b: 255 }
];

#[rustfmt::skip]
const PINK_SIDE: [Pixel;5] = [
    Pixel { r: 241, g: 83, b: 155 },
    Pixel { r: 236, g: 80, b: 153 },
    Pixel { r: 207, g: 58, b: 140 },
    Pixel { r: 151, g: 12, b: 117 },
    Pixel { r: 195, g: 55, b: 131 }
];

#[rustfmt::skip]
const BLINK_SIDE: [Pixel;6] = [
    Pixel { r: 124, g: 244, b: 251 },
    Pixel { r: 78, g: 236, b: 247 },
    Pixel { r: 253, g: 142, b: 255 },
    Pixel { r: 45, g: 223, b: 242 },
    Pixel { r: 127, g: 200, b: 232 },
    Pixel { r: 70, g: 231, b: 245 },
];

#[rustfmt::skip]
const WHITE_SIDE: [Pixel;3] = [
    Pixel { r: 190, g: 189, b: 192 },
    Pixel { r: 112, g: 109, b: 120 },
    Pixel { r: 198, g: 198, b: 198 },
];

/////////////////////////
// BOMB
/////////////////////////
#[rustfmt::skip]
const PURPLE_BOMB: [Pixel;2] = [
    Pixel { r: 156, g: 23, b: 224 },
    Pixel { r: 152, g: 22, b: 219 },
];

#[rustfmt::skip]
const WHITE_BOMB: [Pixel;1] = [
    Pixel { r: 237, g: 237, b: 237 },
];

#[rustfmt::skip]
const YELLOW_BOMB: [Pixel;2] = [
    Pixel { r: 246, g: 201, b: 57 },
    Pixel { r: 248, g: 203, b: 58 },
];

#[rustfmt::skip]
const PINK_BOMB: [Pixel;2] = [
    Pixel { r: 250, g: 87, b: 158 },
    Pixel { r: 252, g: 88, b: 158 },
];

#[rustfmt::skip]
const RED_BOMB: [Pixel;2] = [
    Pixel { r: 230, g: 17, b: 54 },
    Pixel { r: 228, g: 17, b: 54 },
];

#[rustfmt::skip]
const GREEN_BOMB: [Pixel;2] = [
    Pixel { r: 98, g: 199, b: 32 },
    Pixel { r: 97, g: 197, b: 32 },
];

#[rustfmt::skip]
const BLINK_BOMB: [Pixel;2] = [
    Pixel { r: 92, g: 223, b: 219 },
    Pixel { r: 92, g: 222, b: 219 },
];

/////////////////////////
// EGG
/////////////////////////
#[rustfmt::skip]
const YELLOW_EGG: [Pixel;22] = [
    Pixel { r: 254, g: 220, b: 76 },
    Pixel { r: 254, g: 223, b: 87 },
    Pixel { r: 254, g: 226, b: 106 },
    Pixel { r: 254, g: 226, b: 103 },
    Pixel { r: 254, g: 223, b: 90 },
    Pixel { r: 254, g: 221, b: 81 },
    Pixel { r: 254, g: 221, b: 78 },
    Pixel { r: 254, g: 221, b: 83 },
    Pixel { r: 254, g: 222, b: 85 },
    Pixel { r: 254, g: 224, b: 99 },
    Pixel { r: 254, g: 223, b: 95 },
    Pixel { r: 254, g: 224, b: 97 },
    Pixel { r: 254, g: 221, b: 88 },
    Pixel { r: 254, g: 227, b: 100 },
    Pixel { r: 254, g: 225, b: 94 },
    Pixel { r: 254, g: 224, b: 102 },
    Pixel { r: 254, g: 223, b: 92 },
    Pixel { r: 254, g: 224, b: 105 },
    Pixel { r: 254, g: 226, b: 110 },
    Pixel { r: 254, g: 228, b: 112 },
    Pixel { r: 254, g: 227, b: 108 },
    Pixel { r: 254, g: 226, b: 97 },
];

#[rustfmt::skip]
const RED_EGG: [Pixel;11] = [
    Pixel { r: 173, g: 7, b: 55 },
    Pixel { r: 174, g: 10, b: 57 },
    Pixel { r: 175, g: 12, b: 59 },
    Pixel { r: 178, g: 20, b: 65 },
    Pixel { r: 176, g: 17, b: 61 },
    Pixel { r: 178, g: 24, b: 68 },
    Pixel { r: 177, g: 19, b: 63 },
    Pixel { r: 179, g: 26, b: 68 },
    Pixel { r: 175, g: 7, b: 55 },
    Pixel { r: 177, g: 15, b: 61 },
    Pixel { r: 179, g: 17, b: 61 },
];

#[rustfmt::skip]
const WHITE_EGG: [Pixel;3] = [
    Pixel { r: 218, g: 218, b: 218 },
    Pixel { r: 220, g: 220, b: 220 },
    Pixel { r: 216, g: 216, b: 216 },
];

#[rustfmt::skip]
const PURPLE_EGG: [Pixel;8] = [
    Pixel { r: 133, g: 5, b: 193 },
    Pixel { r: 136, g: 9, b: 194 },
    Pixel { r: 135, g: 11, b: 194 },
    Pixel { r: 134, g: 7, b: 193 },
    Pixel { r: 134, g: 13, b: 193 },
    Pixel { r: 134, g: 16, b: 193 },
    Pixel { r: 136, g: 17, b: 195 },
    Pixel { r: 153, g: 20, b: 203 }
];

#[rustfmt::skip]
const GREEN_EGG: [Pixel;5] = [
    Pixel { r: 67, g: 175, b: 0 },
    Pixel { r: 73, g: 177, b: 10 },
    Pixel { r: 69, g: 176, b: 5 },
    Pixel { r: 68, g: 176, b: 2 },
    Pixel { r: 65, g: 174, b: 0 },
];

#[rustfmt::skip]
const PINK_EGG: [Pixel;3] = [
    Pixel { r: 255, g: 97, b: 163 },
    Pixel { r: 255, g: 100, b: 165 },
    Pixel { r: 255, g: 102, b: 166 }
];

#[rustfmt::skip]
const BLINK_EGG: [Pixel;4] = [
    Pixel { r: 98, g: 228, b: 223 },
    Pixel { r: 123, g: 234, b: 229 },
    Pixel { r: 117, g: 232, b: 227 },
    Pixel { r: 106, g: 230, b: 224 },
];

#[rustfmt::skip]
const DBLUE_EGG: [Pixel;5] = [
    Pixel { r: 87, g: 78, b: 253 },
    Pixel { r: 93, g: 84, b: 253 },
    Pixel { r: 100, g: 91, b: 254 },
    Pixel { r: 96, g: 87, b: 253 },
    Pixel { r: 89, g: 79, b: 253 },
];

fn contains_close(pxa: &[Pixel], px: &Pixel) -> bool {
    for p in pxa {
        if px.r == p.r || px.r.min(254) + 1 == p.r || px.r.max(1) - 1 == p.r {
            if px.g == p.g || px.g.min(254) + 1 == p.g || px.g.max(1) - 1 == p.g {
                if px.b == p.b || px.b.min(254) + 1 == p.b || px.b.max(1) - 1 == p.b {
                    return true;
                }
            }
        }
    }
    false
}

#[rustfmt::skip]
pub fn item_from_profile(px: Pixel, hint1: &Pixel, hint2: &Pixel, hint3: &Pixel) -> ItemType {
    if contains_close(&PURPLE_BIRD, &px) {
        ItemType::Normal(Bird::Purple)
    } else if contains_close(&RED_BIRD, &px) {
        ItemType::Normal(Bird::Red)
    } else if contains_close(&PINK_BIRD, &px) {
        ItemType::Normal(Bird::Pink)
    } else if contains_close(&DBLUE_BIRD, &px) {
        if hint1.b >= 254 { // Check close to bg
            ItemType::Normal(Bird::DarkBlue)
        } else {
            ItemType::Bomb(Bird::DarkBlue, false)
        }
    } else if contains_close(&WHITE_BIRD, &px) {
        if hint1.g > 90 && hint1.r > 160 && hint1.b < 100 {
            ItemType::Up(Bird::White)
        } else {
            if hint2.r > 110 && hint2.g > 110 && hint2.r < 200 && hint2.g < 200 && !(hint3.r > 180 && hint3.b < 100) {
                ItemType::Egg(Bird::White)
            } else {
                ItemType::Normal(Bird::White)
            }
        }
    } else if contains_close(&YELLOW_BIRD, &px) {
        if hint2.r > 110 && hint2.g > 40 { // Check not close to bg
            ItemType::Normal(Bird::Yellow)
        } else {
            ItemType::Side(Bird::Yellow)
        }
    } else if contains_close(&BLINK_BIRD, &px) {
        //println!("blink: Checking hint2 {:?}", hint2);
        if hint2.b > 100 && hint2.g > 40 { // Check not close to bg
            ItemType::Normal(Bird::BluePink)
        } else {
            ItemType::Side(Bird::BluePink)
        }
    } else if contains_close(&GREEN_BIRD, &px) {
        ItemType::Normal(Bird::Green)

    } else if contains_close(&YELLOW_UP, &px) || contains_close(&YELLOW_EGG, &px) {
        //println!("yel: Checking hint1 {:?}", hint1);
        if hint1.r > 200 && hint1.g < 140 { // Check if orangish
            ItemType::Up(Bird::Yellow)
        } else {
           //println!("yel: Checking hint2 {:?}", hint2);
            if hint2.r > 110 && hint2.g > 40 { // Check not close to bg
                ItemType::Egg(Bird::Yellow)
            } else {
                ItemType::Side(Bird::Yellow)
            }
        }
    } else if contains_close(&GREEN_UP, &px) {
        ItemType::Up(Bird::Green)
    } else if contains_close(&PURPLE_UP, &px) {
        if hint1.g > 130 && hint1.r > 160 && hint1.b < 100 {
            ItemType::Up(Bird::Purple)
        } else {
            ItemType::Side(Bird::Purple)
        }
    } else if contains_close(&RED_UP, &px) {
        ItemType::Up(Bird::Red)
    } else if contains_close(&PINK_UP, &px) {
        ItemType::Up(Bird::Pink)
    } else if contains_close(&BLINK_UP, &px) {
        ItemType::Up(Bird::BluePink)
    } else if contains_close(&DBLUE_UP, &px) {
        ItemType::Up(Bird::DarkBlue)
    } else if contains_close(&WHITE_UP, &px) {
        if hint1.r > 150 && hint1.b < 100 {
            ItemType::Up(Bird::White)
        } else {
            ItemType::Egg(Bird::White)
        }

    } else if contains_close(&RED_SIDE, &px) {
        ItemType::Side(Bird::Red)
    } else if contains_close(&BLINK_SIDE, &px) {
        ItemType::Side(Bird::BluePink)
    } else if contains_close(&DBLUE_SIDE, &px) {
        ItemType::Side(Bird::DarkBlue)
    } else if contains_close(&PINK_SIDE, &px) {
        ItemType::Side(Bird::Pink)
    } else if contains_close(&WHITE_SIDE, &px) {
        if hint2.r < 110 && hint2.g < 30 { // Check close to bg
            ItemType::Side(Bird::White)
        } else {
            ItemType::Bomb(Bird::White, false)
        }
    } else if contains_close(&GREEN_SIDE, &px) {
        ItemType::Side(Bird::Green)

    } else if contains_close(&WHITE_BOMB, &px) {
        if hint2.r < 110 && hint2.g < 30 { // Check close to bg
            ItemType::Side(Bird::White)
        } else {
            ItemType::Bomb(Bird::White, false)
        }
    } else if contains_close(&YELLOW_BOMB, &px) {
        ItemType::Bomb(Bird::Yellow, false)
    } else if contains_close(&PINK_BOMB, &px) {
        ItemType::Bomb(Bird::Pink, false)
    } else if contains_close(&RED_BOMB, &px) {
        ItemType::Bomb(Bird::Red, false)
    } else if contains_close(&BLINK_BOMB, &px) {
        ItemType::Bomb(Bird::BluePink, false)
    } else if contains_close(&GREEN_BOMB, &px) {
        ItemType::Bomb(Bird::Green, false)
    } else if contains_close(&PURPLE_BOMB, &px) {
        ItemType::Bomb(Bird::Purple, false)

    } else if contains_close(&WHITE_EGG, &px) {
        if hint1.r > 150 && hint1.b < 100 {
            ItemType::Up(Bird::White)
        } else {
            if hint2.r < 110 && hint2.g < 30 { // Check close to bg
                ItemType::Side(Bird::White)
            } else {
                if !(hint3.r > 180 && hint3.b < 100) {
                    ItemType::Egg(Bird::White)
                } else {
                    ItemType::Normal(Bird::White)
                }
            }
        }
    } else if contains_close(&PURPLE_EGG, &px) {
        ItemType::Egg(Bird::Purple)
    } else if contains_close(&PINK_EGG, &px) {
        ItemType::Egg(Bird::Pink)
    } else if contains_close(&BLINK_EGG, &px) {
        ItemType::Egg(Bird::BluePink)
    } else if contains_close(&DBLUE_EGG, &px) {
        ItemType::Egg(Bird::DarkBlue)
    } else if contains_close(&GREEN_EGG, &px) {
        ItemType::Egg(Bird::Green)
    } else if contains_close(&RED_EGG, &px) {
        ItemType::Egg(Bird::Red)

    } else if contains_close(&RAINBOW_BIRD, &px) {
        ItemType::Rainbow
    } else if contains_close(&ITEM, &px) {
        ItemType::Item
    } else {
        ItemType::Unknown
    }
}
