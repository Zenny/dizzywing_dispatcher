use crate::color_profiles::item_from_profile;
use crate::dizzy::{GRID_OFFSET, Dizzy};
use crate::pixelmap::{Pixel, PixelMap};
use rand::{thread_rng, Rng};
use std::fmt;
use std::cmp::Ordering;
use std::time::Duration;
use crate::window::Window;

pub const SQUARE_DIM: u8 = 74;

pub const GRID_SIZE: u8 = 9;

#[derive(Debug, Clone, PartialEq)]
pub enum Bird {
    Red,
    Green,
    Yellow,
    White,
    Purple,
    BluePink,
    DarkBlue,
    Pink,
}

impl fmt::Display for Bird {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Bird::Red => write!(f, "Re"),
            Bird::Green => write!(f, "Gr"),
            Bird::Yellow => write!(f, "Ye"),
            Bird::White => write!(f, "Wh"),
            Bird::Purple => write!(f, "Pu"),
            Bird::BluePink => write!(f, "Bp"),
            Bird::DarkBlue => write!(f, "Db"),
            Bird::Pink => write!(f, "Pi"),
        }
    }
}

#[derive(Debug, Clone)]
pub enum ItemType {
    Normal(Bird),
    Rainbow,
    Up(Bird),
    Side(Bird),
    Bomb(Bird, bool),
    Item,
    Egg(Bird),
    Unknown,
}

impl ItemType {
    pub fn is_unknown(&self) -> bool {
        if let ItemType::Unknown = self {
            true
        } else {
            false
        }
    }

    pub fn is_egg(&self) -> bool {
        if let ItemType::Egg(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_used_bomb(&self) -> bool {
        if let ItemType::Bomb(_, used) = self {
            *used
        } else {
            false
        }
    }

    pub fn is_item(&self) -> bool {
        if let ItemType::Item = self {
            true
        } else {
            false
        }
    }

    pub fn is_always_movable(&self) -> bool {
        match self {
            ItemType::Rainbow => true,
            ItemType::Up(_) => true,
            ItemType::Side(_) => true,
            ItemType::Bomb(_, _) => true,
            _ => false,
        }
    }

    pub fn is_clickable(&self) -> bool {
        match self {
            ItemType::Normal(_) => false,
            ItemType::Rainbow => false,
            ItemType::Up(_) => true,
            ItemType::Side(_) => true,
            ItemType::Bomb(_, false) => true,
            ItemType::Unknown => false,
            _ => false,
        }
    }

    // Is used bomb ignored?
    pub fn inner_ignore_egg(&self) -> Option<Bird> {
        match self {
            ItemType::Normal(b) => Some(b.clone()),
            ItemType::Up(b) => Some(b.clone()),
            ItemType::Side(b) => Some(b.clone()),
            ItemType::Bomb(b, used) => if *used { None } else { Some(b.clone()) },
            _ => None,
        }
    }

    // Is used bomb ignored?
    pub fn inner(&self) -> Option<Bird> {
        match self {
            ItemType::Normal(b) => Some(b.clone()),
            ItemType::Up(b) => Some(b.clone()),
            ItemType::Side(b) => Some(b.clone()),
            ItemType::Bomb(b, used) => if *used { None } else { Some(b.clone()) },
            ItemType::Egg(b) => Some(b.clone()),
            _ => None,
        }
    }
}

impl PartialEq for ItemType {
    fn eq(&self, other: &Self) -> bool {
        use self::ItemType::*;
        match (self, other) {
            (Normal(Bird::White), Normal(Bird::White)) => true,
            (Normal(Bird::Red), Normal(Bird::Red)) => true,
            (Normal(Bird::BluePink), Normal(Bird::BluePink)) => true,
            (Normal(Bird::Green), Normal(Bird::Green)) => true,
            (Normal(Bird::Yellow), Normal(Bird::Yellow)) => true,
            (Normal(Bird::Purple), Normal(Bird::Purple)) => true,

            (Up(Bird::White), Up(Bird::White)) => true,
            (Up(Bird::Red), Up(Bird::Red)) => true,
            (Up(Bird::BluePink), Up(Bird::BluePink)) => true,
            (Up(Bird::Green), Up(Bird::Green)) => true,
            (Up(Bird::Yellow), Up(Bird::Yellow)) => true,
            (Up(Bird::Purple), Up(Bird::Purple)) => true,

            (Side(Bird::White), Side(Bird::White)) => true,
            (Side(Bird::Red), Side(Bird::Red)) => true,
            (Side(Bird::BluePink), Side(Bird::BluePink)) => true,
            (Side(Bird::Green), Side(Bird::Green)) => true,
            (Side(Bird::Yellow), Side(Bird::Yellow)) => true,
            (Side(Bird::Purple), Side(Bird::Purple)) => true,

            (Bomb(Bird::White, _), Bomb(Bird::White, _)) => true,
            (Bomb(Bird::Red, _), Bomb(Bird::Red, _)) => true,
            (Bomb(Bird::BluePink, _), Bomb(Bird::BluePink, _)) => true,
            (Bomb(Bird::Green, _), Bomb(Bird::Green, _)) => true,
            (Bomb(Bird::Yellow, _), Bomb(Bird::Yellow, _)) => true,
            (Bomb(Bird::Purple, _), Bomb(Bird::Purple, _)) => true,

            (Egg(Bird::White), Egg(Bird::White)) => true,
            (Egg(Bird::Red), Egg(Bird::Red)) => true,
            (Egg(Bird::BluePink), Egg(Bird::BluePink)) => true,
            (Egg(Bird::Green), Egg(Bird::Green)) => true,
            (Egg(Bird::Yellow), Egg(Bird::Yellow)) => true,
            (Egg(Bird::Purple), Egg(Bird::Purple)) => true,

            (Rainbow, Rainbow) => true,
            (Item, Item) => true,
            (Unknown, Unknown) => true,
            _ => false,
        }
    }

    fn ne(&self, other: &Self) -> bool {
        !self.eq(other)
    }
}

impl fmt::Display for ItemType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ItemType::Normal(b) => write!(f, "n{}", b),
            ItemType::Rainbow => write!(f, "Rai"),
            ItemType::Up(b) => write!(f, "u{}", b),
            ItemType::Side(b) => write!(f, "s{}", b),
            ItemType::Bomb(b, _) => write!(f, "b{}", b),
            ItemType::Item => write!(f, "Itm"),
            ItemType::Egg(b) => write!(f, "e{}", b),
            ItemType::Unknown => write!(f, "???"),
        }
    }
}

#[derive(Debug, Clone)]
struct BirdMatch {
    pub bird: Option<Bird>,
    pub start: (u8, u8),
    pub count: u8,
    pub collides: Vec<(u8, u8)>,
    pub removed: bool,
}

#[derive(Debug, Clone, Default)]
pub struct LocalMove {
    pub steps: i32,
    pub clears: i32,
    pub specs_made: i32,
    pub goal_prog: i32,
    pub moves: i32,
}

impl PartialOrd for LocalMove {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        let a = ((self.clears + self.specs_made + self.goal_prog) as f64
            + (self.moves as f64 * 0.07))
            / 4.0;
        let b = ((other.clears + other.specs_made + other.goal_prog) as f64
            + (other.moves as f64 * 0.07))
            / 4.0;
        a.partial_cmp(&b)
    }
}

impl PartialEq for LocalMove {
    fn eq(&self, other: &Self) -> bool {
        let a = ((self.clears + self.specs_made + self.goal_prog) as f64
            + (self.moves as f64 * 0.07))
            / 4.0;
        let b = ((other.clears + other.specs_made + other.goal_prog) as f64
            + (other.moves as f64 * 0.07))
            / 4.0;
        a.eq(&b)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct MoveInfo {
    pub pos: (usize, usize),
    pub dir: (i8, i8),
}

#[derive(Debug, Clone)]
pub struct GameGrid {
    pub has_item: bool,
    pub has_egg: bool,
    pub has_unk: bool,
    pub has_used_bomb: bool,
    birds: Vec<ItemType>,
    detonated: Vec<(usize, usize)>,
}

impl GameGrid {
    pub fn new() -> Self {
        let len = GRID_SIZE as usize * GRID_SIZE as usize;
        let mut v = Vec::with_capacity(len);
        for _ in 0..len {
            v.push(ItemType::Unknown);
        }
        Self {
            has_item: false,
            has_egg: false,
            has_unk: true,
            has_used_bomb: false,
            birds: v,
            detonated: vec![],
        }
    }

    pub fn update_real(&mut self, win: &mut Window) {
        let mut bmp = Window::try_get_bitmap(win,
                GRID_OFFSET.0,
                GRID_OFFSET.1,
                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
            );

        let mut pxmap = PixelMap::from(&bmp, usize::from(SQUARE_DIM) * 9);
        self.has_egg = false;
        self.has_item = false;
        self.has_unk = false;
        for x in 0..GRID_SIZE {
            for y in 0..GRID_SIZE {
                let (sq_cx, sq_cy) = (
                    usize::from(SQUARE_DIM) * usize::from(x)
                        + (f64::from(SQUARE_DIM) * 0.5) as usize,
                    usize::from(SQUARE_DIM) * usize::from(y)
                        + (f64::from(SQUARE_DIM) * 0.5) as usize,
                );
                let mut item = ItemType::Unknown;
                let mut failsafe = 0;
                let mut avg = Pixel { r: 0, g: 0, b: 0 };

                while item.is_unknown() && failsafe < 13 {
                    if failsafe > 0 {
                        std::thread::sleep(Duration::from_millis(91));

                        bmp = Window::try_get_bitmap(win,
                                GRID_OFFSET.0,
                                GRID_OFFSET.1,
                                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
                                u32::from(SQUARE_DIM) * u32::from(GRID_SIZE),
                            );

                        pxmap = PixelMap::from(&bmp, usize::from(SQUARE_DIM) * 9);
                    }

                    avg = pxmap.avg_from_range((sq_cx, sq_cy), (8, 8)).unwrap();
                    let bx = usize::from(SQUARE_DIM) * usize::from(x);
                    let by = usize::from(SQUARE_DIM) * usize::from(y);
                    let hint1 = pxmap.get(bx + 39, by + 18).unwrap();
                    let hint2 = pxmap.get(bx + 27, by + 51).unwrap();
                    let hint3 = pxmap.get(bx + 42, by + 35).unwrap();
                    item = item_from_profile(avg, hint1, hint2, hint3);
                    if item.is_unknown() {
                        win.update_pos().expect("Failed to get window pos");
                        println!("Unknown color profile: {:?} at {},{}", avg, x, y);
                    }

                    failsafe += 1;
                }
                if item.is_unknown() {
                    self.has_unk = true;
                    println!("Unknown color profile: {:?} at {},{}", avg, x, y);
                } else if item.is_egg() {
                    self.has_egg = true;
                } else if item.is_item() {
                    self.has_item = true;
                }
                std::mem::replace(
                    &mut self.birds[usize::from(x) + usize::from(y) * usize::from(GRID_SIZE)],
                    item,
                );
            }
        }
    }

    /// Returns the amount of steps the move caused, spaces cleared, and the board is modified
    pub fn play_move_local(&mut self, mvinfo: &MoveInfo) -> LocalMove {
        if mvinfo.dir.0 != 0 && mvinfo.dir.1 != 0 {
            return LocalMove::default(); // Can't move diag
        }

        if (mvinfo.pos.0 == 0 && mvinfo.dir.0 < 0) || (mvinfo.pos.1 == 0 && mvinfo.dir.1 < 0) {
            return LocalMove::default();
        }

        let fullmove = (
            (mvinfo.pos.0 as i8 + mvinfo.dir.0) as usize,
            (mvinfo.pos.1 as i8 + mvinfo.dir.1) as usize,
        );
        if fullmove.0 >= GRID_SIZE as usize || fullmove.1 >= GRID_SIZE as usize {
            return LocalMove::default();
        }

        if let Some(moved) = self.get_cloned(mvinfo.pos.0, mvinfo.pos.1) {
            let on_click = moved.is_clickable();

            if !on_click && mvinfo.dir == (0, 0) {
                return LocalMove::default();
            }

            let mut stepped = true;
            let mut steps = 1;
            let mut goal_effort = 0;
            let swapped = self
                .get_cloned(fullmove.0, fullmove.1)
                .expect("Failed to get swapped item");

            macro_rules! exec_special_swap {
                () => {
                    match swapped {
                        ItemType::Normal(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            0
                        }
                        ItemType::Rainbow => {
                            self.clear_board(); // TODO: This is our best guess for now. Whatever.
                            9 * 9
                        }
                        ItemType::Up(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Side(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Bomb(_, false) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Item => {
                            self.swap(mvinfo.pos, fullmove);
                            0
                        }
                        _ => {
                            // Do nothing
                            0
                        }
                    }
                };
            }

            let moved1 = moved.clone();
            let mut cleared = match moved {
                ItemType::Normal(_) => {
                    match swapped {
                        ItemType::Normal(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            0
                        }
                        ItemType::Rainbow => {
                            self.rem(fullmove.0, fullmove.1); // Remove self
                            self.rem_all(moved1) + 1 // Will even kill a match if there is one
                        }
                        ItemType::Up(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Side(_) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Bomb(_, false) => {
                            self.swap(mvinfo.pos, fullmove);
                            if !self.spot_match(mvinfo.pos.0, mvinfo.pos.1) {
                                self.death_action(mvinfo.pos.0, mvinfo.pos.1)
                            } else {
                                0
                            }
                        }
                        ItemType::Item => {
                            self.swap(mvinfo.pos, fullmove);
                            if mvinfo.pos.1 > fullmove.1 {
                                if self.spot_match(fullmove.0, fullmove.1) {
                                    10
                                } else {
                                    0
                                }
                            } else {
                                0
                            }
                        }
                        _ => {
                            // Do nothing
                            0
                        }
                    }
                }
                ItemType::Rainbow => {
                    match swapped {
                        ItemType::Normal(_) => {
                            self.rem(mvinfo.pos.0, mvinfo.pos.1); // Remove self
                            self.rem_all(swapped) + 1
                        }
                        ItemType::Rainbow
                        | ItemType::Up(_)
                        | ItemType::Side(_)
                        | ItemType::Bomb(_, _) => {
                            self.clear_board(); // TODO: This is our best guess for now. Whatever.
                            9 * 9
                        }
                        ItemType::Item => {
                            // TODO: Item 1 more spot over?
                            0
                        }
                        ItemType::Egg(_) => 0,
                        ItemType::Unknown => 0,
                    }
                }
                ItemType::Up(_) => {
                    let num = exec_special_swap!();
                    if !self.spot_match(fullmove.0, fullmove.1) {
                        self.death_action(fullmove.0, fullmove.1) + num
                    } else {
                        num
                    }
                }
                ItemType::Side(_) => {
                    let num = exec_special_swap!();
                    if !self.spot_match(fullmove.0, fullmove.1) {
                        self.death_action(fullmove.0, fullmove.1) + num
                    } else {
                        num
                    }
                }
                ItemType::Bomb(_, false) => {
                    let num = exec_special_swap!();
                    if !self.spot_match(fullmove.0, fullmove.1) {
                        self.death_action(fullmove.0, fullmove.1) + num
                    } else {
                        num
                    }
                }
                ItemType::Item => {
                    self.swap(mvinfo.pos, fullmove);
                    if fullmove.1 > mvinfo.pos.1 {
                        if self.spot_match(fullmove.0, fullmove.1) {
                            10
                        } else {
                            0
                        }
                    } else {
                        0
                    }
                }
                _ => 0, // Egg & unk & expl bomb
            };
            goal_effort += self.do_physics();

            let mut usermove = Some(fullmove);
            let mut specials = 0;
            while stepped {
                let (moves, specs) = self.do_matches(usermove);
                stepped = moves > 0;
                if stepped {
                    goal_effort += self.do_physics();
                    steps += 1;
                    cleared += moves;
                    specials += specs;
                } else if self.has_used_bomb {
                    for x in 0..GRID_SIZE as usize {
                        for y in 0..GRID_SIZE as usize {
                            if self.get(x, y).unwrap().is_used_bomb() {
                                cleared += self.death_action(x, y);
                            }
                        }
                    }
                    self.has_used_bomb = false;
                    goal_effort += self.do_physics();
                    steps += 1;
                    stepped = true;
                }
                self.detonated = vec![];
                usermove = None;
            }
            LocalMove {
                steps,
                clears: cleared,
                specs_made: specials,
                goal_prog: goal_effort,
                moves: self.find_moves().len() as i32,
            }
        } else {
            LocalMove::default()
        }
    }

    /// Returns None if out of bounds
    pub fn get(&self, x: usize, y: usize) -> Option<&ItemType> {
        self.birds.get(x + y * usize::from(GRID_SIZE))
    }

    pub fn get_cloned(&self, x: usize, y: usize) -> Option<ItemType> {
        self.birds.get(x + y * usize::from(GRID_SIZE)).cloned()
    }

    /// Returns the type replaced
    pub fn replace(&mut self, x: usize, y: usize, it: ItemType) -> ItemType {
        std::mem::replace(&mut self.birds[x + y * usize::from(GRID_SIZE)], it)
    }

    /// Swaps two coords on the grid
    pub fn swap(&mut self, a: (usize, usize), b: (usize, usize)) {
        let bit = self.birds[b.0 + b.1 * usize::from(GRID_SIZE)].clone();
        let ait = std::mem::replace(&mut self.birds[a.0 + a.1 * usize::from(GRID_SIZE)], bit);
        std::mem::replace(&mut self.birds[b.0 + b.1 * usize::from(GRID_SIZE)], ait);
    }

    /// If it's already unknown, returns false
    pub fn rem(&mut self, x: usize, y: usize) -> bool {
        match std::mem::replace(
            &mut self.birds[x + y * usize::from(GRID_SIZE)],
            ItemType::Unknown,
        ) {
            ItemType::Unknown => false,
            _ => true,
        }
    }

    /// Returns amount cleared
    pub fn rem_all(&mut self, it: ItemType) -> i32 {
        let mut ret = 0;
        for i in 0..self.birds.len() {
            let item = self.birds.get(i).unwrap().clone();
            if it == item {
                ret += 1;
                std::mem::replace(&mut self.birds[i], ItemType::Unknown);
            }
            if item.is_egg() {
                let inner = item.inner().unwrap();
                if inner == it.inner().unwrap() {
                    ret += 5; // Benefit reaching goal
                    std::mem::replace(&mut self.birds[i], ItemType::Normal(inner));
                }
            }
        }
        ret
    }

    pub fn clear_board(&mut self) {
        self.birds.clear();
        let len = GRID_SIZE * GRID_SIZE;
        for _ in 0..len {
            self.birds.push(ItemType::Unknown);
        }
    }

    /// Returns amount recursively cleared
    pub fn death_action(&mut self, x: usize, y: usize) -> i32 {
        let mut ret = 0;
        match self.get(x, y).expect("OOB death action").to_owned() {
            ItemType::Normal(_) => {
                self.rem(x, y);
                ret = 1;
            }
            ItemType::Up(_) => {
                self.rem(x, y); // Rem first to avoid infinite recurse
                for ay in 0..GRID_SIZE as usize {
                    ret += self.death_action(x, ay);
                }
                ret += 1;
            }
            ItemType::Side(_) => {
                self.rem(x, y); // Rem first to avoid infinite recurse
                for ax in 0..GRID_SIZE as usize {
                    ret += self.death_action(ax, y);
                }
                ret += 1;
            }
            ItemType::Bomb(bird, used) => {
                // Check if we haven't detonated on this move already
                if !self.detonated.contains(&(x, y)) {
                    if used {
                        self.rem(x, y);
                        ret += 1;
                    } else {
                        self.replace(x, y, ItemType::Bomb(bird.to_owned(), true));
                        //used = true; // TODO: Does this work as expected?
                        self.has_used_bomb = true;
                        self.detonated.push((x, y));
                    }
                    for i in (x as i32 - 1).max(0)..=(x as i32 + 1).min(8) {
                        for j in (y as i32 - 1).max(0)..=(y as i32 + 1).min(8) {
                            if i as usize == x && j as usize == y {
                                continue; // Not ourselves
                            }
                            ret += self.death_action(i as usize, j as usize);
                        }
                    }
                }
            }
            ItemType::Egg(bird) => {
                self.replace(x, y, ItemType::Normal(bird.to_owned()));
                ret += 15;
            }
            _ => {}
        }
        ret
    }

    /// Returns true on AT LEAST a 3 match. No other known match details.
    /// Impossible to be a match without the x,y item
    pub fn spot_match(&self, x: usize, y: usize) -> bool {
        let mut bird_match = BirdMatch {
            bird: None,
            start: (0, 0),
            count: 0,
            collides: vec![],
            removed: false,
        };
        for ya in (y as i32 - 2).max(0)..=(y as i32 + 2).min(8) {
            let it = self.get(x, ya as usize).unwrap();
            if let Some(bird) = it.inner_ignore_egg() {
                if bird_match.bird.is_none() || bird != bird_match.bird.as_ref().unwrap().clone() {
                    bird_match = BirdMatch {
                        bird: it.inner_ignore_egg(),
                        start: (x as u8, ya as u8),
                        count: 1,
                        collides: vec![],
                        removed: false,
                    };
                } else if bird == bird_match.bird.as_ref().unwrap().clone() {
                    bird_match.count += 1;
                    if bird_match.count > 2 {
                        return true;
                    }
                }
            } else if bird_match.count > 0 {
                bird_match = BirdMatch {
                    bird: None,
                    start: (0, 0),
                    count: 0,
                    collides: vec![],
                    removed: false,
                }
            }
        }

        bird_match = BirdMatch {
            bird: None,
            start: (0, 0),
            count: 0,
            collides: vec![],
            removed: false,
        };
        for xa in (x as i32 - 2).max(0)..=(x as i32 + 2).min(8) {
            let it = self.get(xa as usize, y).unwrap();
            if let Some(bird) = it.inner_ignore_egg() {
                if bird_match.bird.is_none() || bird != bird_match.bird.as_ref().unwrap().clone() {
                    bird_match = BirdMatch {
                        bird: it.inner_ignore_egg(),
                        start: (xa as u8, y as u8),
                        count: 1,
                        collides: vec![],
                        removed: false,
                    };
                } else if bird == bird_match.bird.as_ref().unwrap().clone() {
                    bird_match.count += 1;
                    if bird_match.count > 2 {
                        return true;
                    }
                }
            } else if bird_match.count > 0 {
                bird_match = BirdMatch {
                    bird: None,
                    start: (0, 0),
                    count: 0,
                    collides: vec![],
                    removed: false,
                }
            }
        }
        return false;
    }

    /// T, corner, + makes bomb
    /// 4 (no collision) makes random up/side on farthest left (or player move tile)
    /// 5 makes rainbow
    /// TODO: Bomb on s/u bird is triple beam both directions at drag-to spot
    /// TODO: s/u bird on s/u bird is beam in both directions at drag-to spot
    /// Returns spaces cleared, specials made and modifies the board
    pub fn do_matches(&mut self, user_move: Option<(usize, usize)>) -> (i32, i32) {
        let mut mats = 0;
        let mut specs = 0;
        let (mut x_mats, mut y_mats) = self.collect_bird_matches(3);
        // If user move and 4x, spawn at user move spot. Otherwise spawn left-y top-y
        // Bomb spawn at user move or left-y top-y collision

        for xm in &mut x_mats {
            for ym in &mut y_mats {
                if ym.start.0 >= xm.start.0
                    && ym.start.0 < xm.start.0 + xm.count
                    && xm.start.1 >= ym.start.1
                    && xm.start.1 < ym.start.1 + ym.count
                {
                    xm.collides.push(ym.start.to_owned());
                    ym.collides.push(xm.start.to_owned());
                }
            }
        }

        for xm in &mut x_mats {
            if let Some(item) = self.get((xm.start.0 - 1) as usize, xm.start.1 as usize) {
                if item.is_egg() {
                    mats += self.death_action((xm.start.0 - 1) as usize, xm.start.1 as usize);
                }
            }
            if let Some(item) = self.get((xm.start.0 + xm.count) as usize, xm.start.1 as usize) {
                if item.is_egg() {
                    mats +=
                        self.death_action((xm.start.0 + xm.count) as usize, xm.start.1 as usize);
                }
            }
            if let Some(xmc) = xm.collides.get(0) {
                let mut spawn_bomb = true;
                for ym in &mut y_mats {
                    if xm.collides.contains(&ym.start) {
                        // If removed, this chain was already handled
                        if ym.removed {
                            spawn_bomb = false;
                            continue;
                        }
                        if let Some(item) = self.get(ym.start.0 as usize, (ym.start.1 - 1) as usize)
                        {
                            if item.is_egg() {
                                mats += self
                                    .death_action(ym.start.0 as usize, (ym.start.1 - 1) as usize);
                            }
                        }
                        if let Some(item) =
                            self.get(ym.start.0 as usize, (ym.start.1 + ym.count) as usize)
                        {
                            if item.is_egg() {
                                mats += self.death_action(
                                    ym.start.0 as usize,
                                    (ym.start.1 + ym.count) as usize,
                                );
                            }
                        }
                        for y in ym.start.1..ym.start.1 + ym.count {
                            mats += self.death_action(ym.start.0 as usize, y as usize);
                            if let Some(item) = self.get((ym.start.0 + 1) as usize, y as usize) {
                                if item.is_egg() {
                                    mats +=
                                        self.death_action((ym.start.0 + 1) as usize, y as usize);
                                }
                            }
                            if let Some(item) = self.get((ym.start.0 - 1) as usize, y as usize) {
                                if item.is_egg() {
                                    mats +=
                                        self.death_action((ym.start.0 - 1) as usize, y as usize);
                                }
                            }
                        }
                        //mats += ym.count as i32;
                        ym.removed = true;
                    }
                }
                for x in xm.start.0..xm.start.0 + xm.count {
                    mats += self.death_action(x as usize, xm.start.1 as usize);
                    if let Some(item) = self.get(x as usize, (xm.start.1 + 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 + 1) as usize);
                        }
                    }
                    if let Some(item) = self.get(x as usize, (xm.start.1 - 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 - 1) as usize);
                        }
                    }
                }
                xm.removed = true;
                // Subtract amount of collisions, they were removed and accounted
                //mats += xm.count as i32 - i32::try_from(xm.collides.len()).unwrap_or(0);

                if spawn_bomb {
                    specs += 17;
                    if let Some(umove) = user_move {
                        self.replace(
                            umove.0,
                            umove.1,
                            ItemType::Bomb(xm.bird.clone().unwrap(), false),
                        );
                    } else {
                        self.replace(
                            xmc.0 as usize,
                            xm.start.1 as usize,
                            ItemType::Bomb(xm.bird.clone().unwrap(), false),
                        );
                    }
                }
            } else if xm.count == 4 {
                // TODO: If bomb moved in, spawn special above left after exploding/match
                for x in xm.start.0..xm.start.0 + xm.count {
                    mats += self.death_action(x as usize, xm.start.1 as usize);
                    if let Some(item) = self.get(x as usize, (xm.start.1 + 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 + 1) as usize);
                        }
                    }
                    if let Some(item) = self.get(x as usize, (xm.start.1 - 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 - 1) as usize);
                        }
                    }
                }
                xm.removed = true;
                //mats += 4;
                specs += 9;

                let mut rng = thread_rng();
                let spawn = if rng.gen_bool(0.5) {
                    ItemType::Up(xm.bird.clone().unwrap())
                } else {
                    ItemType::Side(xm.bird.clone().unwrap())
                };

                if let Some(umove) = user_move {
                    self.replace(umove.0, umove.1, spawn);
                } else {
                    self.replace(xm.start.0 as usize, xm.start.1 as usize, spawn);
                }
            } else if xm.count >= 5 {
                for x in xm.start.0..xm.start.0 + xm.count {
                    mats += self.death_action(x as usize, xm.start.1 as usize);
                    if let Some(item) = self.get(x as usize, (xm.start.1 + 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 + 1) as usize);
                        }
                    }
                    if let Some(item) = self.get(x as usize, (xm.start.1 - 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 - 1) as usize);
                        }
                    }
                }
                xm.removed = true;
                //mats += xm.count as i32;
                specs += 35;

                if let Some(umove) = user_move {
                    self.replace(umove.0, umove.1, ItemType::Rainbow);
                } else {
                    self.replace(xm.start.0 as usize, xm.start.1 as usize, ItemType::Rainbow);
                }
            } else {
                // Match 3
                for x in xm.start.0..xm.start.0 + xm.count {
                    mats += self.death_action(x as usize, xm.start.1 as usize);
                    if let Some(item) = self.get(x as usize, (xm.start.1 + 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 + 1) as usize);
                        }
                    }
                    if let Some(item) = self.get(x as usize, (xm.start.1 - 1) as usize) {
                        if item.is_egg() {
                            mats += self.death_action(x as usize, (xm.start.1 - 1) as usize);
                        }
                    }
                }
                xm.removed = true;
                //mats += 3;
            }
        }

        for ym in &mut y_mats {
            if ym.removed {
                continue;
            }
            if let Some(item) = self.get(ym.start.0 as usize, (ym.start.1 - 1) as usize) {
                if item.is_egg() {
                    mats += self.death_action(ym.start.0 as usize, (ym.start.1 - 1) as usize);
                }
            }
            if let Some(item) = self.get(ym.start.0 as usize, (ym.start.1 + ym.count) as usize) {
                if item.is_egg() {
                    mats +=
                        self.death_action(ym.start.0 as usize, (ym.start.1 + ym.count) as usize);
                }
            }
            if ym.count == 4 {
                // TODO: If bomb moved in, spawn special above left after exploding/match
                for y in ym.start.1..ym.start.1 + ym.count {
                    mats += self.death_action(ym.start.0 as usize, y as usize);
                    if let Some(item) = self.get((ym.start.0 + 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 + 1) as usize, y as usize);
                        }
                    }
                    if let Some(item) = self.get((ym.start.0 - 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 - 1) as usize, y as usize);
                        }
                    }
                }
                ym.removed = true;
                //mats += 4;
                specs += 9;

                let mut rng = thread_rng();
                let spawn = if rng.gen_bool(0.5) {
                    ItemType::Up(ym.bird.clone().unwrap())
                } else {
                    ItemType::Side(ym.bird.clone().unwrap())
                };

                if let Some(umove) = user_move {
                    self.replace(umove.0, umove.1, spawn);
                } else {
                    self.replace(ym.start.0 as usize, ym.start.1 as usize, spawn);
                }
            } else if ym.count >= 5 {
                for y in ym.start.1..ym.start.1 + ym.count {
                    mats += self.death_action(ym.start.0 as usize, y as usize);
                    if let Some(item) = self.get((ym.start.0 + 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 + 1) as usize, y as usize);
                        }
                    }
                    if let Some(item) = self.get((ym.start.0 - 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 - 1) as usize, y as usize);
                        }
                    }
                }
                ym.removed = true;
                //mats += ym.count as i32;
                specs += 35;

                if let Some(umove) = user_move {
                    self.replace(umove.0, umove.1, ItemType::Rainbow);
                } else {
                    self.replace(ym.start.0 as usize, ym.start.1 as usize, ItemType::Rainbow);
                }
            } else {
                // Match 3
                for y in ym.start.1..ym.start.1 + ym.count {
                    mats += self.death_action(ym.start.0 as usize, y as usize);
                    if let Some(item) = self.get((ym.start.0 + 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 + 1) as usize, y as usize);
                        }
                    }
                    if let Some(item) = self.get((ym.start.0 - 1) as usize, y as usize) {
                        if item.is_egg() {
                            mats += self.death_action((ym.start.0 - 1) as usize, y as usize);
                        }
                    }
                }
                ym.removed = true;
                //mats += 3;
            }
        }

        (mats, specs)
    }

    fn collect_bird_matches(&self, min: u8) -> (Vec<BirdMatch>, Vec<BirdMatch>) {
        let mut x_mats = Vec::new();
        let mut y_mats = Vec::new();
        for x in 0..GRID_SIZE {
            let mut bird_match = BirdMatch {
                bird: None,
                start: (0, 0),
                count: 0,
                collides: vec![],
                removed: false,
            };
            for y in 0..GRID_SIZE {
                let it = self.get(x as usize, y as usize).unwrap();
                if let Some(bird) = it.inner_ignore_egg() {
                    if bird_match.bird.is_none()
                        || bird != bird_match.bird.as_ref().unwrap().clone()
                    {
                        if bird_match.count >= min {
                            y_mats.push(bird_match);
                        }
                        bird_match = BirdMatch {
                            bird: it.inner_ignore_egg(),
                            start: (x, y),
                            count: 1,
                            collides: vec![],
                            removed: false,
                        };
                    } else if bird == bird_match.bird.as_ref().unwrap().clone() {
                        bird_match.count += 1;
                    }
                } else if bird_match.count > 0 {
                    if bird_match.count >= min {
                        y_mats.push(bird_match);
                    }
                    bird_match = BirdMatch {
                        bird: None,
                        start: (0, 0),
                        count: 0,
                        collides: vec![],
                        removed: false,
                    }
                }
            }
            if bird_match.count >= min {
                y_mats.push(bird_match);
            }
        }

        for y in 0..GRID_SIZE {
            let mut bird_match = BirdMatch {
                bird: None,
                start: (0, 0),
                count: 0,
                collides: vec![],
                removed: false,
            };
            for x in 0..GRID_SIZE {
                let it = self.get(x as usize, y as usize).unwrap();
                if let Some(bird) = it.inner_ignore_egg() {
                    if bird_match.bird.is_none()
                        || bird != bird_match.bird.as_ref().unwrap().clone()
                    {
                        if bird_match.count >= min {
                            x_mats.push(bird_match);
                        }
                        bird_match = BirdMatch {
                            bird: it.inner_ignore_egg(),
                            start: (x, y),
                            count: 1,
                            collides: vec![],
                            removed: false,
                        };
                    } else if bird == bird_match.bird.as_ref().unwrap().clone() {
                        bird_match.count += 1;
                    }
                } else if bird_match.count > 0 {
                    if bird_match.count >= min {
                        x_mats.push(bird_match);
                    }
                    bird_match = BirdMatch {
                        bird: None,
                        start: (0, 0),
                        count: 0,
                        collides: vec![],
                        removed: false,
                    }
                }
            }
            if bird_match.count >= min {
                x_mats.push(bird_match);
            }
        }

        (x_mats, y_mats)
    }

    /// Returns special move bonus (Items)
    pub fn do_physics(&mut self) -> i32 {
        let mut ret = 0;
        for x in 0..usize::from(GRID_SIZE) {
            for y in (0..usize::from(GRID_SIZE)).rev() {
                if let Some(it) = self.get_cloned(x, y) {
                    if it.is_unknown() {
                        let mut i = 1;
                        let mut mv = ItemType::Unknown;
                        while mv.is_unknown() && i <= y {
                            mv = std::mem::replace(
                                &mut self.birds[x + (y - i) * usize::from(GRID_SIZE)],
                                ItemType::Unknown,
                            );
                            i += 1;
                        }
                        if mv.is_unknown() {
                            continue;
                        }
                        if mv.is_item() {
                            ret += 25 * (i as i32 - 1); // Bonus per x moves
                            if y == usize::from(GRID_SIZE) - 1 {
                                std::mem::replace(
                                    &mut self.birds[x + y * usize::from(GRID_SIZE)],
                                    ItemType::Unknown,
                                );
                                ret += self.do_physics() + 15; // Plus a goal clear bonus
                                return ret; // Does this work?
                            } else {
                                std::mem::replace(
                                    &mut self.birds[x + y * usize::from(GRID_SIZE)],
                                    mv,
                                );
                            }
                        } else {
                            std::mem::replace(&mut self.birds[x + y * usize::from(GRID_SIZE)], mv);
                        }
                    }
                }
            }
        }
        ret
    }

    pub fn find_moves(&self) -> Vec<MoveInfo> {
        let mut ret = Vec::new();

        let (x_mats, y_mats) = self.collect_bird_matches(2);

        for mat in x_mats {
            if mat.bird.is_none() {
                continue;
            }
            if mat.start.0 > 0 {
                if mat.start.1 > 0 {
                    let pos = ((mat.start.0 - 1) as usize, (mat.start.1 - 1) as usize);
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (0, 1) });
                    }
                }
                if mat.start.1 < GRID_SIZE - 1 {
                    let pos = ((mat.start.0 - 1) as usize, (mat.start.1 + 1) as usize);
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (0, -1) });
                    }
                }
            }
            if mat.start.0 > 1 {
                let pos = ((mat.start.0 - 2) as usize, mat.start.1 as usize);
                let move1 = self.get(pos.0, pos.1).unwrap();
                if mat.bird == move1.inner_ignore_egg() {
                    ret.push(MoveInfo { pos, dir: (1, 0) });
                }
            }
            if mat.start.0 + mat.count < GRID_SIZE {
                if mat.start.1 > 0 {
                    let pos = (
                        (mat.start.0 + mat.count) as usize,
                        (mat.start.1 - 1) as usize,
                    );
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (0, 1) });
                    }
                }
                if mat.start.1 < GRID_SIZE - 1 {
                    let pos = (
                        (mat.start.0 + mat.count) as usize,
                        (mat.start.1 + 1) as usize,
                    );
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (0, -1) });
                    }
                }
            }
            if mat.start.0 + mat.count < GRID_SIZE - 1 {
                let pos = ((mat.start.0 + mat.count + 1) as usize, mat.start.1 as usize);
                let move1 = self.get(pos.0, pos.1).unwrap();
                if mat.bird == move1.inner_ignore_egg() {
                    ret.push(MoveInfo { pos, dir: (-1, 0) });
                }
            }
        }

        for mat in y_mats {
            if mat.bird.is_none() {
                continue;
            }
            if mat.start.1 > 0 {
                if mat.start.0 > 0 {
                    let pos = ((mat.start.0 - 1) as usize, (mat.start.1 - 1) as usize);
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (1, 0) });
                    }
                }
                if mat.start.0 < GRID_SIZE - 1 {
                    let pos = ((mat.start.0 + 1) as usize, (mat.start.1 - 1) as usize);
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (-1, 0) });
                    }
                }
            }
            if mat.start.1 > 1 {
                let pos = (mat.start.0 as usize, (mat.start.1 - 2) as usize);
                let move1 = self.get(pos.0, pos.1).unwrap();
                if mat.bird == move1.inner_ignore_egg() {
                    ret.push(MoveInfo { pos, dir: (0, 1) });
                }
            }
            if mat.start.1 + mat.count < GRID_SIZE {
                if mat.start.0 > 0 {
                    let pos = (
                        (mat.start.0 - 1) as usize,
                        (mat.start.1 + mat.count) as usize,
                    );
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (1, 0) });
                    }
                }
                if mat.start.0 < GRID_SIZE - 1 {
                    let pos = (
                        (mat.start.0 + 1) as usize,
                        (mat.start.1 + mat.count) as usize,
                    );
                    let move1 = self.get(pos.0, pos.1).unwrap();
                    if mat.bird == move1.inner_ignore_egg() {
                        ret.push(MoveInfo { pos, dir: (-1, 0) });
                    }
                }
            }
            if mat.start.1 + mat.count < GRID_SIZE - 1 {
                let pos = (mat.start.0 as usize, (mat.start.1 + mat.count + 1) as usize);
                let move1 = self.get(pos.0, pos.1).unwrap();
                if mat.bird == move1.inner_ignore_egg() {
                    ret.push(MoveInfo { pos, dir: (0, -1) });
                }
            }
        }

        for x in 0..GRID_SIZE as usize {
            for y in 0..GRID_SIZE as usize {
                let item = self.get(x, y).unwrap();
                let inner = item.inner_ignore_egg();
                if item.is_egg() {
                    continue;
                }
                if x < (GRID_SIZE - 2) as usize {
                    let itemx = self.get(x + 2, y).unwrap();
                    if inner == itemx.inner_ignore_egg() {
                        if y > 0 {
                            let itemy = self.get(x + 1, y - 1).unwrap();
                            if inner == itemy.inner_ignore_egg() {
                                let mi = MoveInfo {
                                    pos: (x + 1, y - 1),
                                    dir: (0, 1),
                                };
                                if !ret.contains(&mi) {
                                    ret.push(mi);
                                }
                            }
                        }
                        if y < (GRID_SIZE - 1) as usize {
                            let itemy = self.get(x + 1, y + 1).unwrap();
                            if inner == itemy.inner_ignore_egg() {
                                let mi = MoveInfo {
                                    pos: (x + 1, y + 1),
                                    dir: (0, -1),
                                };
                                if !ret.contains(&mi) {
                                    ret.push(mi);
                                }
                            }
                        }
                    }
                }
                if y < (GRID_SIZE - 2) as usize {
                    let itemy = self.get(x, y + 2).unwrap();
                    if inner == itemy.inner_ignore_egg() {
                        if x > 0 {
                            let itemx = self.get(x - 1, y + 1).unwrap();
                            if inner == itemx.inner_ignore_egg() {
                                let mi = MoveInfo {
                                    pos: (x - 1, y + 1),
                                    dir: (1, 0),
                                };
                                if !ret.contains(&mi) {
                                    ret.push(mi);
                                }
                            }
                        }
                        if x < (GRID_SIZE - 1) as usize {
                            let itemx = self.get(x + 1, y + 1).unwrap();
                            if inner == itemx.inner_ignore_egg() {
                                let mi = MoveInfo {
                                    pos: (x + 1, y + 1),
                                    dir: (-1, 0),
                                };
                                if !ret.contains(&mi) {
                                    ret.push(mi);
                                }
                            }
                        }
                    }
                }
                if item.is_always_movable() {
                    if x > 0 {
                        let iteml = self.get(x - 1, y).unwrap();
                        if !iteml.is_egg() {
                            let mi = MoveInfo {
                                pos: (x, y),
                                dir: (-1, 0),
                            };
                            if !ret.contains(&mi) {
                                ret.push(mi);
                            }
                        }
                    }
                    if x < (GRID_SIZE - 1) as usize {
                        let itemr = self.get(x + 1, y).unwrap();
                        if !itemr.is_egg() {
                            let mi = MoveInfo {
                                pos: (x, y),
                                dir: (1, 0),
                            };
                            if !ret.contains(&mi) {
                                ret.push(mi);
                            }
                        }
                    }
                    if y > 0 {
                        let itemu = self.get(x, y - 1).unwrap();
                        if !itemu.is_egg() {
                            let mi = MoveInfo {
                                pos: (x, y),
                                dir: (0, -1),
                            };
                            if !ret.contains(&mi) {
                                ret.push(mi);
                            }
                        }
                    }
                    if y < (GRID_SIZE - 1) as usize {
                        let itemd = self.get(x, y + 1).unwrap();
                        if !itemd.is_egg() {
                            let mi = MoveInfo {
                                pos: (x, y),
                                dir: (0, 1),
                            };
                            if !ret.contains(&mi) {
                                ret.push(mi);
                            }
                        }
                    }
                }
                if item.is_clickable() {
                    ret.push(MoveInfo {
                        pos: (x, y),
                        dir: (0, 0),
                    });
                }
            }
        }

        ret
    }
}

impl fmt::Display for GameGrid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut ret = "[ ".to_string();
        let mut i = 0;

        for b in &self.birds {
            if i % GRID_SIZE == 0 && i > 0 {
                ret.push_str("\n[ ");
            }
            if i % GRID_SIZE == GRID_SIZE - 1 && i > 0 {
                ret.push_str(&format!("{} ]", b));
            } else {
                ret.push_str(&format!("{} | ", b));
            }

            i += 1;
        }

        write!(f, "{}", ret)
    }
}
