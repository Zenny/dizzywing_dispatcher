///! Reserved
mod window;
use window::Window;

mod dizzy;
use dizzy::Dizzy;

mod pixelmap;

mod gamegrid;

mod color_profiles;

use crate::window::WinFindError;

use std::thread::sleep;
use std::time::Duration;

use inputbot::{handle_input_events, KeybdKey, MouseButton};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

pub fn is_null<T>(a: *mut T) -> bool {
    a == std::ptr::null_mut()
}

pub fn get_win() -> Window {
    match Window::from_title("Feral") {
        Ok(v) => {
            if v.area.left < 0 || v.area.top < 0 {
                println!("ERROR: Window is outside of your screen. {:?}", v.area);
                sleep(Duration::from_secs(6));
                quit::with_code(-1);
            }
            if v.area.width != 1024 || v.area.height != 768 {
                println!("ERROR: Window is not 1024x768. {:?}", v.area);
                sleep(Duration::from_secs(6));
                quit::with_code(-1);
            }
            v
        }
        Err(e) => {
            match e {
                WinFindError::WinNotFound => {
                    println!(
                        "ERROR: Couldn't find Feral window. \
                        Boot Feral and try again. While you're at it, \
                        set the window size to 1024x768."
                    );
                }
                WinFindError::RectNotFound => {
                    println!("ERROR: Couldn't find window dimensions.");
                }
            }
            sleep(Duration::from_secs(6));
            quit::with_code(-1);
        }
    }
}

#[quit::main]
fn main() {
    let should_stop = Arc::new(AtomicBool::new(false));
    ctrlc::set_handler(move || match Window::from_title("Feral") {
        Ok(v) => {
            v.topmost(false);
        }
        Err(e) => {
            match e {
                WinFindError::WinNotFound => {
                    println!(
                        "ERROR: Couldn't find Feral window. \
                        Boot Feral and try again. While you're at it, \
                        set the window size to 1024x768."
                    );
                }
                WinFindError::RectNotFound => {
                    println!("ERROR: Couldn't find window dimensions.");
                }
            }
            sleep(Duration::from_secs(6));
        }
    })
    .expect("Error setting Ctrl-C handler");

    let win = get_win();

    println!("Found {:?}", win);
    println!("NOTICE: Game will become a TOPMOST window!");
    println!("Preparing game area...");

    let mut game = Dizzy::new(win, should_stop.clone());
    println!("Game {:?}", game);
    std::thread::sleep(Duration::from_secs(3));

    let _esc = std::thread::spawn(move || {
        KeybdKey::EscapeKey.bind(move || {
            println!("Exited with keybind");
            should_stop.store(true, Ordering::SeqCst);
            MouseButton::LeftButton.release();
        });

        handle_input_events();
    });

    game.play();

    //MouseCursor.move_abs(0,0);
    //MouseCursor.move_rel(game.grid_area.left, game.grid_area.top);
}
